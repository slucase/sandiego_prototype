﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneMapButton : MonoBehaviour {
	public Button button;

	void Update()
	{

	}

	public void MapButtonTrigger(){
		this.GetComponentInParent<MapManager>().ChangeScene(this.button);
	}

	public void OnMouseOver(){
		this.GetComponentInParent<MapManager>().UpdateMapPrompt(this.button);
		Debug.Log("mouse over ok");
	}
	public void OnMouseExit(){
		this.GetComponentInParent<MapManager>().UpdateMapPrompt();
		Debug.Log("mouse exit ok");
	}


	
}
