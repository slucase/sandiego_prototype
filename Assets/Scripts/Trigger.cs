﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Trigger : MonoBehaviour {

	public GameObject person;

	private bool discovered = false;

	public void TriggerButton(){
		TriggerPrompt();
		TriggerInventory();

	}
	

	public void TriggerPrompt (){
		
		FindObjectOfType<PromptManager>().UpdatePrompt(person.GetComponent<PersonButton>().GetPerson());

//	InventoryManager._inventory.AddToDb(person.GetComponent<PersonButton>().GetPerson());

	}

	public void TriggerInventory(){
		if (!discovered){
			FindObjectOfType<_Gameplay>().UpdateTimeInventory();
			discovered = true;
		}
	}


}
