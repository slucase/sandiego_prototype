﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getMetadata : MonoBehaviour {
    public string placeName;
    public string placeDescription;

    public string destinationScene01;
    public string destinationName01;

    public string destinationScene02;
    public string destinationName02;

    public string destinationScene03;
    public string destinationName03;

    public string personText01;
    public string personText02;
    public string personText03;


    public string Name()
    {
        return placeName;
    }

    public string Description() {
        return placeDescription;
	}

    public string destinationScene(int sceneNumber)
    {
        IList<string> Places = new List<string>() { destinationScene01, destinationScene02, destinationScene03 };
        return Places[sceneNumber - 1];
    }

    public string destinationName(int sceneNumber)
    {
        IList<string> Places = new List<string>() { destinationName01, destinationName02, destinationName03 };
        return Places[sceneNumber - 1];
    }

    public string PersonText(int personNumber)
    {
        IList<string> Places = new List<string>() { personText01, personText02, personText03 };
        return Places[personNumber - 1];
    }

//    public string PersonText(int personNumber)
//   {
//        return personTexts[personNumber - 1];
//    }


}
