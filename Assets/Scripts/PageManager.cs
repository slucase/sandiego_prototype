﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PageManager : MonoBehaviour {

	private int pageNumber = 0;

	public GameObject _gamedata;
	public GameObject nextPage, prevPage;

	public Text[] DataEntry;

	public Text pageDisplay;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		pageDisplay.text = (pageNumber + 1).ToString();
		UpdatePrevPage();
		UpdateNextPage();
		UpdateCurrentPage();
	}

	public void NextPage(){
		pageNumber++;
	}
	public void PreviousPage(){
		pageNumber--;
	}

	private void UpdatePrevPage() {
		if (pageNumber <= 0){
			prevPage.SetActive(false);
		}
		else {
			prevPage.SetActive(true);
		}

	}

	private void UpdateNextPage() {
		if (_gamedata.GetComponent<_Gamedata>().EntriesNumber() <= (pageNumber + 1)*DataEntry.Length) {
			nextPage.SetActive(false);
		}
		else {
			nextPage.SetActive(true);
		}
	}

	private void UpdateCurrentPage(){
		if (_gamedata.GetComponent<_Gamedata>().EntriesNumber() > 0){
			Debug.Log("DE LEngth = " + DataEntry.Length.ToString());
			for (int i = 0; i < DataEntry.Length; i++)
			{
				DataEntry[i].text = (_gamedata.GetComponent<_Gamedata>().GetEntryText(i+pageNumber*(DataEntry.Length-1)));
				//DataEntry[i].text = (InventoryManager._inventory.GetEntryText(i+pageNumber*(DataEntry.Length-1)));
			}
		}
	}
}
