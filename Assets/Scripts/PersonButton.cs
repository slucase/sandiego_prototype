﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersonButton : MonoBehaviour {

	private Clues person;
//	public Text props;
	public Button button;

//	public GameObject prefab = null;

	public void SetPerson(Clues person){
		this.person = person;
		this.button.GetComponent<Image>().sprite = person.icon;
	}

	public Clues GetPerson(){
		return this.person;

	}
/*
	public void InstatiateButton(Person levelPerson) {
		
		GameObject pb = Instantiate(prefab);
		
		if (pb != null){
			pb.GetComponent<PersonButton>().SetPerson(levelPerson);
		}
	}
*/
}
