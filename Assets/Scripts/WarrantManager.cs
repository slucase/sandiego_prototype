﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarrantManager : MonoBehaviour {

	public Dropdown[] dropdowns;
	public Text[] titles;
	public GameObject _gamedata;

	public bool IsSuspectCorrect(int suspectNumber){
		return (
			        (this.dropdowns[0].captionText.text == _gamedata.GetComponent<_Gamedata>().RetrieveSexEntry(suspectNumber)) &&
			        (this.dropdowns[1].captionText.text == _gamedata.GetComponent<_Gamedata>().RetrieveGameEntry(suspectNumber)) &&
			        (this.dropdowns[2].captionText.text == _gamedata.GetComponent<_Gamedata>().RetrieveFilmEntry(suspectNumber)) &&
			        (this.dropdowns[3].captionText.text == _gamedata.GetComponent<_Gamedata>().RetrieveBandEntry(suspectNumber))
			    );
	}
	void Start () {
		SetDropdown();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SetDropdown ()  {
		this.dropdowns[0].AddOptions(_gamedata.GetComponent<_Gamedata>().RetrieveSexEntries());
		this.dropdowns[1].AddOptions(_gamedata.GetComponent<_Gamedata>().RetrieveGameEntries());
		this.dropdowns[2].AddOptions(_gamedata.GetComponent<_Gamedata>().RetrieveFilmEntries());
		this.dropdowns[3].AddOptions(_gamedata.GetComponent<_Gamedata>().RetrieveBandEntries());

		this.titles[0].text = "Sex";
		this.titles[1].text = "Game";
		this.titles[2].text = "Film";
		this.titles[3].text = "Band";

	}

	
		
}
