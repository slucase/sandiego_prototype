﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



[System.Serializable]
public class Clues {

	public string title;

   [TextArea(3, 10)]
	public string message;

	public Sprite icon;

/* 
	public void SetProperties(string name, string hobby, string author, string gender, string orientation, string sign){
		
		this.name = name;
		this.hobby = hobby;
		this.author = author;
		this.gender = gender;
		this.orientation = orientation;
		this.sign = sign;
	}
*/
	public string GetTestimony(){
		return (
			"Name: " + this.title + "\n" +
			this.message			
			);
	}



}
