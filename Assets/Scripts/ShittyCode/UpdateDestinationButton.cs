﻿using UnityEngine;
using UnityEngine.UI;

public class UpdateDestinationButton : MonoBehaviour
{
    public int buttonNumber;
    public Text buttonText;
    public getMetadata Place;

    void Update()
    {
        buttonText.text = Place.destinationName(buttonNumber);
    }
}
