﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaceManager : MonoBehaviour {
	public bool isDummy;
	public string placeName;
	public Text placeTitle;

	private bool initialized = false;

	void Start () {
		this.placeTitle.text = this.placeName;
	
	}
	public bool IsInitializedTrue(){
		return this.initialized;
	}

	public void SetInitializedTrue(){
		this.initialized = true;
	}
	

	public string GetPlaceTitle(){
		return this.placeName;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public bool IsDummy(){
		return this.isDummy;
	}
}
