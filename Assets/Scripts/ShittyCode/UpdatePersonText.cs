﻿using UnityEngine.UI;
using UnityEngine;

public class UpdatePersonText : MonoBehaviour
{
    public int personNumber;
    public Text personText;
    public getMetadata Place;


    void Update()
    {
        personText.text = Place.PersonText(personNumber);
    }


}
