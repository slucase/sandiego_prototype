﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToScene : MonoBehaviour {

    public getMetadata Place;
    public int buttonNumber;

    public void ChangeScene()
    {
        SceneManager.LoadScene(Place.destinationScene(buttonNumber - 1));

    }
}
