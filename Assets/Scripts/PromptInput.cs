﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PromptInput {

	public string name;
	
	[TextArea(3, 10)]
	public string prompt;

	public Villain person;
	

}
