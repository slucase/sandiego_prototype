﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class _Gameplay : MonoBehaviour {
	
	public static _Gameplay _gameplay;

	public _Gamedata _gameData;

	public GameObject warrantManager;

	public GameObject game;
	public GameObject gameLost;
	public GameObject gameWon;
	public GameObject prefab;
	public List<GameObject> placeList;

	private List<Clues> dummyClues;
	private List<Clues> trackClues;

	public List<GameObject> scenario;

	public int suspectNumber;

	public int timeLeft;
	public int inventoryPenalty;
	private int gameEnd;

	private int buttonPerLevel;
	void Awake()
	{
		CreateGameplay();
		buttonPerLevel = _gameData.buttonPerLevel;
	}


	public void SetButtonPerLevel(){
		this.buttonPerLevel = _gameData.GetButtonPerLevel();
	}
	public int GetButtonPerLevel(){
		return this.buttonPerLevel;
	}
	private void Start()
	{
		SetClues();
		SetButtonPerLevel();
		InstatiateButtons();
	}

	void SetClues(){
		dummyClues = _gameData.GetDummyClues();
		trackClues = _gameData.GetTrackClues();
	}
	void CreateGameplay(){
		if (_gameplay != null) {
			Destroy(gameObject);
		}
		else {
			_gameplay = this;
			DontDestroyOnLoad(gameObject);
		}
	}
    public void DeactivateAllScenes(){
//		Debug.Log("Received Go To Scene Command to = " + sceneName);
		for (int i = 0; i < placeList.Count; i++){
			placeList[i].SetActive(false);
		}
	}

	public void ActivateScene(int i){
		placeList[i].SetActive(true);
	}
    public int GetTimeLeft(){
		return this.timeLeft;
	}
	public int UpdateTimeInventory(){
		this.timeLeft += this.inventoryPenalty;
		return this.timeLeft;
	}
	
	public void GameWon(){
			this.game.SetActive(false);
		    this.gameWon.SetActive(true);
	}

	public bool IsSupectFound(){
		if(warrantManager.GetComponent<WarrantManager>().IsSuspectCorrect(suspectNumber)){
			return true;
		}
		else {
			return false;
		}
	}
	public void GameLost(){
		this.game.SetActive(false);
		this.gameLost.SetActive(true);
	}
	public void CheckGameEnd(){
		if(IsSupectFound()){
			GameWon();
		}
		if(this.timeLeft <= 0){
			GameLost();
		}

	}
	// Update is called once per frame
	void Update () {
		CheckGameEnd();
	}

	public void InstatiateButtons() {

		int scenarioIndex = 0;
		while (scenarioIndex < scenario.Count){
			for (int i = 0; i < _gameplay.GetButtonPerLevel(); i++) {
				Debug.Log("checking");
			
			    GameObject pb = Instantiate(prefab /*, transform.position, Quaternion.identity*/) as GameObject;
			
			    if (pb != null){
			        pb.transform.parent = scenario[scenarioIndex].transform;
				    if(placeList[i].GetComponent<PlaceManager>().IsDummy() && i < _gameplay.dummyClues.Count){
					    pb.GetComponent<PersonButton>().SetPerson(_gameplay.dummyClues[0]);
				    	_gameplay.dummyClues.RemoveAt(0);
			    	}
			    	else if (i < _gameplay.trackClues.Count){
			    		pb.GetComponent<PersonButton>().SetPerson(_gameplay.trackClues[0]);
			    		_gameplay.trackClues.RemoveAt(0);
			    	}
				}
			}
			scenarioIndex++;
		}
	}
}
