﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapManager : MonoBehaviour {

	public List<Button> placeButtons;
	public List<GameObject> places;

	public Text prompt;

	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void UpdateMapPrompt(){
		this.prompt.text = "";
	}
	public void UpdateMapPrompt(Button button){
		int buttonIndex = this.placeButtons.IndexOf(button);
		this.prompt.text = this.places[buttonIndex].GetComponent<PlaceManager>().GetPlaceTitle();
		Debug.Log("hoover esta chegando no Update Map Prompt");
	}
	public void ClearMapPrompt(){
		this.prompt.text = "";
	}

	public void ChangeScene(Button button){
		_Gameplay._gameplay.DeactivateAllScenes();
		int buttonIndex = this.placeButtons.IndexOf(button);
		_Gameplay._gameplay.ActivateScene(buttonIndex);
	}

}
