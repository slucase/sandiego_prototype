﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _Gamedata : MonoBehaviour {

	public Villain goal;
	public Villain[] villain;
	public GameObject prefab = null;

	public List<Clues> dummyClues;
	public List<Clues> trackClues;

	public int buttonPerLevel;
	
	void Awake() {
	}

	public List<Clues> GetDummyClues() {
		return this.dummyClues;
	}
	public List<Clues> GetTrackClues() {
		return this.trackClues;
	}
	public Villain GetPerson (int i){
		return villain[i];
	}

	public int GetSize(){
		return villain.Length;
	}

	public Villain GetGoal(){
		return this.goal;
	}

	public int GetButtonPerLevel(){
		return buttonPerLevel;
	}

	public string GetEntryText(int entryNumber){
		return villain[entryNumber].GetProperties(); 
	}

	public int EntriesNumber(){
		return villain.Length;
	}
	public string RetrieveSexEntry(int i)
	{
		return villain[i].sex;
	}
	public List<string> RetrieveSexEntries()
	{
		var entryList = new List<string>();
		for (int i = 0; i < EntriesNumber(); i++){
			if (!entryList.Contains(villain[i].sex)){
				entryList.Add(villain[i].sex);
			}
		}
		return entryList;
	}
	public string RetrieveGameEntry(int i)
	{
		return villain[i].videogame;
	}
	public List<string> RetrieveGameEntries()
	{
		var entryList = new List<string>();
		for (int i = 0; i < EntriesNumber(); i++){
			if (!entryList.Contains(villain[i].videogame)){
				entryList.Add(villain[i].videogame);
			}
		}
		return entryList;
	}
	public string RetrieveFilmEntry(int i)
	{
		return villain[i].film;
	}
	public List<string> RetrieveFilmEntries()
	{
		var entryList = new List<string>();
		for (int i = 0; i < EntriesNumber(); i++){
			if (!entryList.Contains(villain[i].film)){
				entryList.Add(villain[i].film);
			}
		}
		return entryList;
	}
	public string RetrieveBandEntry(int i)
	{
		return villain[i].band;
	}
	public List<string> RetrieveBandEntries()
	{
		var entryList = new List<string>();
		for (int i = 0; i < EntriesNumber(); i++){
			if (!entryList.Contains(villain[i].band)){
				entryList.Add(villain[i].band);
			}
		}
		return entryList;
	}
}
